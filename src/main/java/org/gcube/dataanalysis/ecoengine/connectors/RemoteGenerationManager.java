package org.gcube.dataanalysis.ecoengine.connectors;

import org.gcube.contentmanagement.graphtools.utils.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteGenerationManager {

	private static Logger logger = LoggerFactory.getLogger(RemoteGenerationManager.class);
	
	private final String submissionMethod = "submit";
	private final String statusMethod = "status/";
	private String submissionID;
	private String username;
	private String endpoint;
	
	public RemoteGenerationManager(String generatorEndPoint){
		if (generatorEndPoint.charAt(generatorEndPoint.length()-1)=='/')
			endpoint = generatorEndPoint;
		else
			endpoint = generatorEndPoint+"/"; 
	}
	
	public void submitJob(RemoteHspecInputObject rhio) throws Exception{
		
		logger.warn("RemoteGenerationManager: retrieving job information");
		RemoteHspecOutputObject rhoo = null;
		username = rhio.userName;
		try{	
			rhoo = (RemoteHspecOutputObject)HttpRequest.postJSonData(endpoint+submissionMethod, rhio, RemoteHspecOutputObject.class);
			logger.trace("RemoteGenerationManager: job information retrieved");	
		}catch(Exception e){
			e.printStackTrace();
			logger.trace("RemoteGenerationManager: ERROR - job information NOT retrieved");
			throw e;
		}
		if ((rhoo!=null) && (rhoo.id!=null)){
			logger.warn("RemoteGenerationManager: job ID retrieved ");
			submissionID = rhoo.id;
		}
		else{
			logger.warn("RemoteGenerationManager: ERROR - job ID NOT retrieved "+rhoo.error);
			throw new Exception("RemoteGenerationManager: ERROR - job ID NOT retrieved "+rhoo.error);
		}
	}
	
	public double retrieveCompletion(){
		RemoteHspecOutputObject rhoo = retrieveCompleteStatus();
		
		try{
			double completion = Double.parseDouble(rhoo.completion);
			return completion;
		}catch(Exception e){
			logger.warn("RemoteGenerationManager: ERROR - cannot retrieve information from remote site ",e);
		}
		return 0;
	}
	
	public RemoteHspecOutputObject retrieveCompleteStatus(){
		RemoteHspecOutputObject rhoo = null;

		try{
			rhoo = (RemoteHspecOutputObject)HttpRequest.getJSonData(endpoint+statusMethod+submissionID, null ,RemoteHspecOutputObject.class);
		}catch(Exception e){
			logger.warn("RemoteGenerationManager: ERROR - cannot retrieve information from remote site ",e);
		}
		
		return rhoo;
	}
	
	
}
