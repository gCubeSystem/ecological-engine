package org.gcube.dataanalysis.ecoengine.interfaces;

import org.gcube.dataanalysis.ecoengine.configuration.ALG_PROPS;
import org.gcube.dataanalysis.ecoengine.configuration.AlgorithmConfiguration;

public interface Model extends AlgorithmDescriptor{
	
	public ALG_PROPS[] getProperties();
	
	public String getName();
	
	public float getVersion();

	public void setVersion(float version);
	
	public void init(AlgorithmConfiguration Input, Model previousModel);
	
	public String getResourceLoad();
	
	public String getResources();
	
	public float getStatus();
	
	public void postprocess(AlgorithmConfiguration Input, Model previousModel);
	
	public void train(AlgorithmConfiguration Input, Model previousModel);

	public void stop();
	
}
