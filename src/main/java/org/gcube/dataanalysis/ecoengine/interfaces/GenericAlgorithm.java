package org.gcube.dataanalysis.ecoengine.interfaces;

import org.gcube.dataanalysis.ecoengine.configuration.ALG_PROPS;

public interface GenericAlgorithm extends AlgorithmDescriptor {
			
			//defines the properties of this algorithm
			public ALG_PROPS[] getProperties();
			
			//defines the name of this algorithm
			public String getName();
			
}
