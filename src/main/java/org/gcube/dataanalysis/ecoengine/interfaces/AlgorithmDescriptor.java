package org.gcube.dataanalysis.ecoengine.interfaces;

import java.util.List;

import org.gcube.dataanalysis.ecoengine.datatypes.StatisticalType;

public interface AlgorithmDescriptor {
	
	//gets the description of the algorithm
	public String getDescription();
	
	//set the input parameters for this generator
	public List<StatisticalType> getInputParameters();
	
	public StatisticalType getOutput();
}
