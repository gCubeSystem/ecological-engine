This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "ecological-engine"


## [v1.14.1]

- Updated bom to latest version [#24209]


## [v1.14.0] - 2021-01-20

- maryTTS removed
- upgrade repository definition
- added log4j-over-sl4j dependency


## [v1.13.0] - 2020-06-10

- Updated for support https protocol [#19423]


## [v1.10.0] - 2016-27-09

- Changed basic signature and http functions


## [v1.1.0] - 2016-10-03

- First Release

